package com.nadeen.colud.resource.server.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @GetMapping("/secured")
    public String get() {
        return "Welcome!";
    }
}
